// import 'dart:js_util';

import 'package:flutter/material.dart';
import 'package:weather_forecast_page/my_flutter_app_icons.dart';
// import 'package:flutter/my_flutter_app_icons.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(WeatherForecastPage());
}

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.brown[900],
        ),
        textTheme: TextTheme(
          bodyText2: TextStyle(color: Colors.brown[900]),
          subtitle1: TextStyle(color: Colors.brown[900]),
        ),
        iconTheme: IconThemeData(
          color: Colors.brown[900],
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.brown[900],
        ),
    );
  }
}


class WeatherForecastPage extends StatefulWidget {
  @override
  State<WeatherForecastPage> createState() => _WeatherForecastPageState();
}

class _WeatherForecastPageState extends State<WeatherForecastPage> {
  double? spacew = 25;
  double? sizew = 20;
  var currentTheme = APP_THEME.DARK;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        theme: currentTheme == APP_THEME.DARK
            ? MyAppTheme.appThemeLight()
            : MyAppTheme.appThemeDark(),
        home: Scaffold(
          appBar: buildAppBarWidget(),
          body: buildBodyWidget(spacew, sizew, currentTheme),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
              ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
              });
            },
            child: currentTheme == APP_THEME.DARK
                ? Icon(Icons.light_mode)
                : Icon(Icons.dark_mode),
            backgroundColor: currentTheme == APP_THEME.DARK
                ? Colors.brown.shade700
                : Colors.white60,
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,

        )
    );
  }
}

AppBar buildAppBarWidget(){
  return AppBar(
    title: Text("Weather Forecast"),
    leading: IconButton(
        onPressed: (){},
        icon: Icon(
          Icons.list,
          // color: Colors.black,
        )
    ),
    // Icon(
    //   Icons.sunny_snowing,
    //   // color: Colors.black,
    // ),
    // backgroundColor: Colors.amber[100],
    // backgroundColor: Colors.brown[900],
    // actions: <Widget>[
    //   IconButton(
    //       onPressed: (){},
    //       icon: Icon(
    //         Icons.list,
    //         // color: Colors.black,
    //       )
    //   ),
    // ],
  );
}

Widget buildBodyWidget(spacew, sizew, currentTheme) {
  return ListView (
      children: <Widget>[
        Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 220,
                    child: currentTheme == APP_THEME.DARK
                        ? Image.network(
                            //"https://nordicapis.com/wp-content/uploads/5-Best-Free-and-Paid-Weather-APIs-2019-e1587582023501.png",
                            "https://t4.ftcdn.net/jpg/04/61/23/23/360_F_461232389_XCYvca9n9P437nm3FrCsEIapG4SrhufP.jpg",
                            fit: BoxFit.cover,
                          )
                        : Image(
                            image: AssetImage('fonts/bg.png'),
                            fit: BoxFit.cover,
                          ),
                  ),
                  Positioned(
                    child: Container(
                      height: 60,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(left: 8.0),
                            child: Text("เมืองชลบุรี",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), //"Ishigami Senku"
                            ),
                          ),
                          Icon(Icons.location_on_outlined),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 130,
                    child: Container(
                      height: 60,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(left: 8.0,top: 8.0),
                            child: Text("26",
                              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold), //"Ishigami Senku"
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 190,
                    child: Container(
                      height: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(left: 8.0, bottom: 2.0),
                            child: Text("แจ่มใส",
                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold), //"Ishigami Senku"
                            ),
                          ),
                          IconButton(
                              onPressed: (){},
                              icon: Icon(
                                Icons.navigate_next,
                                size: 15,
                                // color: Colors.black,
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: 35,
                child: ListTile(
                  title: Text('วันนี้ วันอังคาร', style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold),),
                  trailing: Text('20~29 ํC', style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold),),
                ),
              ),
              Divider(
                color: Colors.blueGrey[300],
              ),
              Container(
                height: 70.0,
                child: buildListForcast24(spacew, sizew),
              ),
              Divider(
                color: Colors.blueGrey[300],
              ),
              Container(
                height: 120.0,
                child: buildTableForcastfif(),
              ),
              Divider(
                color: Colors.blueGrey[300],
              ),
              Container(
                height: 95,
                child: buildOtherForcastWidget(sizew),
              ),
              Divider(
                color: Colors.blueGrey[300],
              ),
            ]
        ),
      ]
  );
}

Widget buildTableForcastfif(){
  return ListView(
    children: <Widget>[
      Table(
        // defaultColumnWidth: FixedColumnWidth(120.0),
        // border: TableBorder.all(
        //     color: Colors.black,
        //     style: BorderStyle.solid,
        //     width: 2),
        columnWidths: {
          0: FlexColumnWidth(1.3),
          1: FlexColumnWidth(2.8),
          2: FlexColumnWidth(3),
          3: FlexColumnWidth(3),
        },
        children: [
          TableRow(children: [
            Column(children:[Text('28/12', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Text('พุธ', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Icon(CustomIcons.cloud_sun, color: Colors.amber, size: 15)]),
            // Row(mainAxisAlignment: MainAxisAlignment.center, children:[Icon(Icons.sunny, color: Colors.amber, size: 15,),SizedBox(width: 4),Icon(Icons.cloud, color: Colors.lightBlue, size: 15,)]),
            Column(children:[Text('21~30 ํC', style: TextStyle(fontSize: 12.0))]),
          ]),
          TableRow( children: [
            Column(children:[Text('29/12', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Text('พฤหัสบดี', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Icon(CustomIcons.cloud_sun, color: Colors.amber, size: 15)]),
            // Row(mainAxisAlignment: MainAxisAlignment.center, children:[Icon(Icons.sunny, color: Colors.amber, size: 15,),SizedBox(width: 4),Icon(Icons.cloud, color: Colors.lightBlue, size: 15,)]),
            Column(children:[Text('21~30 ํC', style: TextStyle(fontSize: 12.0))]),
          ]),
          TableRow( children: [
            Column(children:[Text('30/12', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Text('ศุกร์', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Icon(Icons.sunny, color: Colors.amber, size: 15,)]),
            Column(children:[Text('20~30 ํC', style: TextStyle(fontSize: 12.0))]),
          ]),
          TableRow( children: [
            Column(children:[Text('31/12', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Text('เสาร์', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Icon(Icons.sunny, color: Colors.amber, size: 15,)]),
            Column(children:[Text('21~29 ํC', style: TextStyle(fontSize: 12.0))]),
          ]),
          TableRow( children: [
            Column(children:[Text('01/01', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Text('อาทิตย์', style: TextStyle(fontSize: 12.0))]),
            Column(children:[Icon(CustomIcons.cloud_sun, color: Colors.amber, size: 15)]),
            // Row(mainAxisAlignment: MainAxisAlignment.center, children:[Icon(Icons.sunny, color: Colors.amber, size: 15,),SizedBox(width: 4),Icon(Icons.cloud, color: Colors.lightBlue, size: 15,)]),
            Column(children:[Text('18~30 ํC', style: TextStyle(fontSize: 12.0))]),
          ]),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(left: 8.0, bottom: 2.0),
            child: Text("การพยากรณ์อากาศสำหรับช่วง 15 วันข้างหน้า",
              style: TextStyle(fontSize: 12,), //"Ishigami Senku"
            ),
          ),
          IconButton(
              onPressed: (){},
              icon: Icon(
                Icons.navigate_next,
                size: 15,
                // color: Colors.black,
              )
          ),
        ],
      ),
    ],
  );
}

Widget buildOtherForcastWidget(sizew){
  // double? sizew = 20;
  return ListView(

    children: <Widget>[
      Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Icon(CustomIcons.temperatire, color: Colors.redAccent, size: sizew,),
              Text('อุณหภูมิที่รู้สึกได้ 31 ํC', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
          Column(
            children: <Widget>[
              Icon(CustomIcons.droplet, color: Colors.blue, size: sizew,),
              Text('ความชื้น 38%', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
          Column(
            children: <Widget>[
              Icon(CustomIcons.mountain, color: Colors.green, size: sizew,),
              Text('ทัศนวิสัย 24 กม.', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
        ],
      ),
      SizedBox(height: 10,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Icon(CustomIcons.wind, color: Colors.indigoAccent[100], size: sizew,),
              Text('    ENE 13 กม./ชม.     ', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
          Column(
            children: <Widget>[
              Icon(CustomIcons.sun, color: Colors.amber[800], size: sizew,),
              Text('    ยูวี อ่อน   ', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
          Column(
            children: <Widget>[
              Icon(Icons.wifi_tethering, color: Colors.lightBlueAccent, size: sizew,),
              Text('ความกดอากาศ', style: TextStyle(
                fontSize: 11,)),
              Text('1015 hPa', style: TextStyle(
                fontSize: 11,)),
            ],
          ),
        ],
      ),
    ],
  );
}

Widget buildListForcast24(spacew, sizew){
  // double? spacew = 25;
  // double? sizew = 20;
  return ListView(
    // This next line does the trick.
    scrollDirection: Axis.horizontal,
    children: <Widget>[
      SizedBox(width: 10),
      Column(
        children: <Widget>[
          Text('12', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[600], size: sizew,),
          Text('26 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('13', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_sun, color: Colors.amber, size: sizew),
          //Icon(Icons.sunny, color: Colors.amber, size: sizew,),
          //Icon(Icons.cloud, color: Colors.lightBlue[300], size: sizew,),
          Text('28 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('14', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_sun, color: Colors.amber, size: sizew),
          // Icon(Icons.sunny, color: Colors.amber, size: sizew,),
          // Icon(Icons.cloud, color: Colors.lightBlue[300], size: sizew,),
          Text('29 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('15', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_sun, color: Colors.amber[400], size: sizew),
          // Icon(Icons.sunny, color: Colors.amber[400], size: sizew,),
          // Icon(Icons.cloud, color: Colors.lightBlue[400], size: sizew,),
          Text('28 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('16', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_sun, color: Colors.amber[400], size: sizew),
          // Icon(Icons.sunny, color: Colors.amber[300], size: sizew,),
          // Icon(Icons.cloud, color: Colors.lightBlue[400], size: sizew,),
          Text('28 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('17', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_sun, color: Colors.amber[300], size: sizew),
          // Icon(Icons.sunny, color: Colors.amber[200], size: sizew,),
          // Icon(Icons.cloud, color: Colors.lightBlue[500], size: sizew,),
          Text('27 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('17:57', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.fog_sun, color: Colors.blue[600], size: 25),
          Text('พระอาทิตย์ตก', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('18', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          // Icon(Icons.nights_stay, color: Colors.indigoAccent, size: sizew,),
          Text('26 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('19', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('25 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('20', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('25 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('21', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('24 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('22', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('23 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('23', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('23 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('00', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('23 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('01', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('22 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('02', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('22 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('03', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('21 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('04', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('21 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('05', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.nightlight, color: Colors.amberAccent, size: sizew,),
          Text('21 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('06', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.cloud_moon, color: Colors.indigoAccent, size: sizew,),
          Text('20 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('06:37', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(CustomIcons.sunrise, color: Colors.amber, size: 30),
          Text('พระอาทิตย์ขึ้น', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('07', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[300], size: sizew,),
          Text('21 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('08', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[400], size: sizew,),
          Text('22 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('09', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[500], size: sizew,),
          Text('24 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('10', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[600], size: sizew,),
          Text('26 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: spacew),
      Column(
        children: <Widget>[
          Text('11', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
          Icon(Icons.sunny, color: Colors.amber[600], size: sizew,),
          Text('27 ํC', style: TextStyle(
              fontSize: 11, fontWeight: FontWeight.bold)),
        ],
      ),
      SizedBox(width: 10),
    ],
  );
}
